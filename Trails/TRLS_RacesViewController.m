//
//  TRLS_RacesViewController.m
//  Trails
//
//  Created by A. Vatsaev on 15/02/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import "TRLS_RacesViewController.h"

#import "TRLS_RaceViewController.h"

@interface TRLS_RacesViewController ()

@end

@implementation TRLS_RacesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.api = [TRLS_API1 alloc].init;
    self.races = [self.api getRaces];
    NSLog(@"%@",self.races);
    
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if([segue.identifier  isEqual: @"RaceViewSegue"]){
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        TRLS_RaceViewController * dst = (TRLS_RaceViewController*)[segue destinationViewController];
        dst.race_id = [self.races[indexPath.row][@"id"] intValue];
        dst.title = self.races[indexPath.row][@"title"];
        dst.description = self.races[indexPath.row][@"description"];
        dst.startTime = self.races[indexPath.row][@"datetime"];
        dst.town = self.races[indexPath.row][@"town"];
        dst.coach = self.races[indexPath.row][@"user"][@"username"];
        dst.participationsCount = [NSString stringWithFormat:@"%lu",
                                   (unsigned long)[self.races[indexPath.row][@"participations"] count]];
    

    }
    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [self.races count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    //cell.textLabel.text = self.races[[NSString stringWithFormat:@"%ld",indexPath.row+1]][@"title"];
    cell.textLabel.text =[NSString stringWithFormat:@"%@ - %@", self.races[indexPath.row][@"title"] , self.races[indexPath.row][@"town"]];
    
    
    NSString* description = self.races[indexPath.row][@"description"];
    NSRange stringRange = {0, MIN([description length], 30)};
    stringRange = [description rangeOfComposedCharacterSequencesForRange:stringRange];
    
    cell.detailTextLabel.text = description;
    
    
    // Configure the cell...
    
    return cell;
}


- (IBAction)reloadList:(id)sender {
    self.races = [self.api getRaces];
    [self.tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
