//
//  TRLS_ProfileViewController.h
//  Trails
//
//  Created by A. Vatsaev on 04/03/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRLS_ProfileViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *usernameView;
@property (strong, nonatomic) IBOutlet UILabel *firstLastNameView;

@property (strong, nonatomic) IBOutlet UILabel *emailView;

@property (strong, nonatomic) IBOutlet UILabel *userTypeView;
@property (strong, nonatomic) IBOutlet UITextView *userBioView;

@property (strong, nonatomic) NSUserDefaults *userDefaults;

@end
