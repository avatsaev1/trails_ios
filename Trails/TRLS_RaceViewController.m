//
//  TRLS_RaceViewController.m
//  Trails
//
//  Created by A. Vatsaev on 15/02/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import "TRLS_RaceViewController.h"
#import "TRLS_LoginViewController.h"
#import "TRLS_RaceMapViewController.h"

@interface TRLS_RaceViewController ()

@end

@implementation TRLS_RaceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.api = [TRLS_API1 alloc].init;
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    self.titleLbl.text = self.titleStr;
    self.descriptionTxt.text = self.description;
    self.startTimeLbl.text = self.startTime;
    self.townLbl.text = self.town;
    self.coachLbl.text = self.coach;
    self.participationsCountLbl.text = self.participationsCount;
    
    [self checkParticipation];
    
	// Do any additional setup after loading the view.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier  isEqual: @"MapViewSG"]){
        TRLS_RaceMapViewController * dest = (TRLS_RaceMapViewController*)[segue destinationViewController];
        dest.raceID = self.race_id;
    }
}

- (IBAction)toggleParticipation:(UIBarButtonItem*)sender {
    
    
    
    
    UIStoryboard *storyBoard = [self storyboard];
    
    TRLS_LoginViewController * loginView =  [storyBoard
                                             instantiateViewControllerWithIdentifier:@"LoginScreen"];
    
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"userauthtoken"] == nil){
        
        [self presentViewController:loginView animated:YES completion:nil];
        
        return;
        
    }
    
    
    
    if(!self.user_participates){
        
        NSDictionary * resp = [self.api acceptParticipationToRace:self.race_id
                              withAuthToken:[[NSUserDefaults standardUserDefaults] stringForKey:@"userauthtoken"]];
        
        NSLog(@"---------- %@",resp);
        
        if([resp[@"status"][@"type"]  isEqual: @"ok"]){
            self.user_participates = YES;
            self.toggleParticipationBtn.title = @"Reject";
        }
        
    }else{
        
        NSDictionary * resp = [self.api rejectParticipationToRace:self.race_id
                                                    withAuthToken:[[NSUserDefaults standardUserDefaults] stringForKey:@"userauthtoken"]];
        
        NSLog(@"~~~~~~~~~~~~~~~ %@",resp);
        if([resp[@"status"][@"type"]  isEqual: @"ok"]){
            self.user_participates = NO;
            self.toggleParticipationBtn.title = @"Participate";
        }
        
    }
    
}


-(void)checkParticipation{
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"userauthtoken"] != nil){
        
        NSDictionary* resp = [self.api getUserParticipationToRace: self.race_id
                                                    withAuthToken:[[NSUserDefaults standardUserDefaults] stringForKey:@"userauthtoken"]];
        
        NSLog(@"\\\\\\\\\\\\\\\\\\\\\\\\\\ %@",resp);
        
        if([resp[@"status"][@"ERRNO"] isEqual:@"ENOENT"]){
            self.user_participates = NO;
            self.toggleParticipationBtn.title = @"Participate";
        }else{
            self.user_participates = YES;
            self.toggleParticipationBtn.title = @"Reject";
        }
        
        
    }else{
        
        
        self.user_participates = false;
        self.toggleParticipationBtn.title = @"Participate";
    }

    
}



- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
