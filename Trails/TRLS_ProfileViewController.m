//
//  TRLS_ProfileViewController.m
//  Trails
//
//  Created by A. Vatsaev on 04/03/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import "TRLS_ProfileViewController.h"
#import "TRLS_LoginViewController.h"

@interface TRLS_ProfileViewController ()

@end

@implementation TRLS_ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    
    UIStoryboard *storyBoard = [self storyboard];
    
    TRLS_LoginViewController * loginView =  [storyBoard
                                             instantiateViewControllerWithIdentifier:@"LoginScreen"];
    
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"userauthtoken"] == nil){
        
        [self presentViewController:loginView animated:YES completion:nil];
        
    }else{
        //self.usernameView.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"authtoken"];
    }
    
    
    
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"Completed login");
    if ( [[NSUserDefaults standardUserDefaults] stringForKey:@"userauthtoken"] == nil) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        self.usernameView.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];
        self.firstLastNameView.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"userlastname"];
        self.emailView.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"useremail"];
        
        if([[[NSUserDefaults standardUserDefaults] stringForKey:@"usergroup"]  isEqual: @"7"]){
            self.userTypeView.text = @"Runner";
            
        }else{
            self.userTypeView.text = @"Coach";
        }
        
        self.userBioView.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"userbio"];
        
        
    }
    
    
}

- (IBAction)logout:(id)sender {
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
