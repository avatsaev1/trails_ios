//
//  TRLS_API1.m
//  Trails
//
//  Created by A. Vatsaev on 14/02/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import "TRLS_API1.h"

@implementation TRLS_API1


-(id)init{
    self = [super init];
    
    if(self){
        NSLog(@"%@",[[UIDevice currentDevice] model]);
        
        if([[[UIDevice currentDevice] model] isEqualToString:@"iPad Simulator"]){
            
            self->APIHost = @"http://localhost:8888/Trails/trails/public/api1/";
            self.apiKey = @"8f8f647a78104a5085967562d98f0d88";
            
            return self;
        }else{
            
            self->APIHost = @"http://192.168.0.17:8888/Trails/trails/public/api1/";
            self.apiKey = @"8f8f647a78104a5085967562d98f0d88";
            
            return self;
            
        }
        
        
    }else{
        return nil;
    }
    
}

-(id)initWithApiKey: (NSString*)apiKey{
    
    self=[self init];
    self.apiKey = apiKey;
    return self;
    
}

-(NSArray*)getRaces{
    
    
    NSURL *endpoint =[NSURL URLWithString:
                      [NSString stringWithFormat:@"%@%@?api_key=%@",
                       self->APIHost,
                       @"races",
                       self.apiKey ]];
    
    NSURLRequest *re= [NSURLRequest requestWithURL:endpoint];
    
    NSURLResponse *resp;
    
    NSError *error;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:re returningResponse:&resp error:&error];
    NSLog(@"Err:\n %@",error);
    
    if(error != nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Couldn't connect to server..." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        return nil;
    }
    
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    
    //NSLog(@"Err:\n %@",error);
    //NSLog(@"%@",dict);
    
    
    
    //NSLog(@"%@",values);
    
    
    return [dict allValues];
    
    
    
    
}


-(NSArray*)getRace: (NSInteger) race_id{
    NSURL *endpoint =[NSURL URLWithString:
                      [NSString stringWithFormat:@"%@%@%d?api_key=%@",
                       self->APIHost,
                       @"race/",
                       (int)(race_id),
                       self.apiKey]];
    
    NSURLRequest *re= [NSURLRequest requestWithURL:endpoint];
    NSURLResponse *resp;
    
    NSError *error;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:re returningResponse:&resp error:&error];
    
    if(error != nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Couldn't connect to server..." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        return nil;
    }
    
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    NSLog(@"Err:\n %@",error);
    //NSLog(@"%@",dict);
    
    return [dict allValues];
}


-(NSDictionary*)getParticipations: (NSString *)token{
    
    NSURL *endpoint =[NSURL URLWithString:
                      [NSString stringWithFormat:@"%@%@?api_key=%@&token=%@",
                       self->APIHost,
                       @"user_participations",
                       self.apiKey,
                       token]];
    
    NSURLRequest *re= [NSURLRequest requestWithURL:endpoint];
    NSURLResponse *resp;
    
    NSError *error;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:re returningResponse:&resp error:&error];
    NSLog(@"Err:\n %@",error);
    
    if(error != nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Couldn't connect to server..." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        return nil;
    }
    
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    NSLog(@"Err:\n %@",error);
    //NSLog(@"%@",dict);
    
    return dict;
    
}

-(NSDictionary*)loginWithUsername:(NSString*)username andPassword:(NSString*)password{
    // /api1/login?api_key=8f8f647a78104a5085967562d98f0d88&username=Trainer&password=Azrwnd79
    NSURL *endpoint =[NSURL URLWithString:
                      [NSString stringWithFormat:@"%@%@?api_key=%@&username=%@&password=%@",
                       self->APIHost,
                       @"login",
                       self.apiKey,
                       username,
                       password ]];
    
    NSURLRequest *re= [NSURLRequest requestWithURL:endpoint];
    
    NSURLResponse *resp;
    
    NSError *error;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:re returningResponse:&resp error:&error];
    
    
    if(error != nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Couldn't connect to server..." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        return nil;
    }
    
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    
    //NSLog(@"Err:\n %@",error);
    //NSLog(@"%@",dict);
    
    
    
    //NSLog(@"%@",values);
    
    
    return dict;
    
}


-(NSDictionary*)acceptParticipationToRace: (NSInteger)race_id withAuthToken:(NSString*)token{
    // /race/:id/accept_participation
    
    
    NSURL *endpoint =[NSURL URLWithString:
                      [NSString stringWithFormat:@"%@%@%d/%@?api_key=%@&token=%@",
                       self->APIHost,
                       @"race/",
                       (int)(race_id),
                       @"accept_participation",
                       self.apiKey,
                       token]
                      ];
    
    NSURLRequest *re= [NSURLRequest requestWithURL:endpoint];
    NSURLResponse *resp;
    
    NSError *error;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:re returningResponse:&resp error:&error];
    
    
    if(error != nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Couldn't connect to server..." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        return nil;
    }
    
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    NSLog(@"Err:\n %@",error);
    NSLog(@"%@",dict);
    
    return dict;

    
}


-(NSDictionary*)rejectParticipationToRace: (NSInteger)race_id withAuthToken:(NSString*)token{
    // /race/:id/accept_participation
    
    
    NSURL *endpoint =[NSURL URLWithString:
                      [NSString stringWithFormat:@"%@%@%d/%@?api_key=%@&token=%@",
                       self->APIHost,
                       @"race/",
                       (int)(race_id),
                       @"reject_participation",
                       self.apiKey,
                       token]
                      ];
    
    NSURLRequest *re= [NSURLRequest requestWithURL:endpoint];
    NSURLResponse *resp;
    
    NSError *error;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:re returningResponse:&resp error:&error];
    
    if(error != nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Couldn't connect to server..." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        return nil;
    }
    
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    NSLog(@"Err:\n %@",error);
    NSLog(@"%@",dict);
    
    return dict;
    
    
}

-(NSDictionary*)getUserParticipationToRace: (NSInteger)race_id withAuthToken:(NSString*)token{
    // /race/:id/accept_participation
    
    
    NSURL *endpoint =[NSURL URLWithString:
                      [NSString stringWithFormat:@"%@%@%d/%@?api_key=%@&token=%@",
                       self->APIHost,
                       @"race/",
                       (int)(race_id),
                       @"user_participation",
                       self.apiKey,
                       token]
                      ];
    
    NSURLRequest *re= [NSURLRequest requestWithURL:endpoint];
    NSURLResponse *resp;
    
    NSError *error;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:re returningResponse:&resp error:&error];
    
    if(error != nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Couldn't connect to server..." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        return nil;
    }
    
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    NSLog(@"Err:\n %@",error);
    NSLog(@"%@",dict);
    
    return dict;
    
    
}


-(NSDictionary*)getRaceGpx: (NSInteger) race_id{
    // /api1/race/12/gpx?api_key=KEY
    
    NSURL *endpoint =[NSURL URLWithString:
                      [NSString stringWithFormat:@"%@%@%d/%@?api_key=%@",
                       self->APIHost,
                       @"race/",
                       (int)(race_id),
                       @"gpx",
                       self.apiKey]];
    
    NSURLRequest *re= [NSURLRequest requestWithURL:endpoint];
    NSURLResponse *resp;
    
    NSError *error;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:re returningResponse:&resp error:&error];
    
    if(error != nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Couldn't connect to server..." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        return nil;
    }
    
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    NSLog(@"Err:\n %@",error);
    //NSLog(@"%@",dict);
    
    return dict;
}




@end
