//
//  TRLS_RaceMapViewController.m
//  Trails
//
//  Created by A. Vatsaev on 08/03/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import "TRLS_RaceMapViewController.h"
#import "GPXTrackPolyLine.h"
#import "TRLS_API1.h"

@interface TRLS_RaceMapViewController ()

@end

@implementation TRLS_RaceMapViewController{
    TRLS_API1 *api;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"------------------------------------------------------------");
    
    api = [[TRLS_API1 alloc] initWithApiKey:@"8f8f647a78104a5085967562d98f0d88"];
    
    
    //NSString *path = [[NSBundle mainBundle] pathForResource:@"track" ofType:@"gpx"];
    //NSString *track = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    //GPXRoot *root = [GPXParser parseGPXWithString:track];
    //NSLog(@"%@",root);
    //NSLog(@"%@",track);
	// Do any additional setup after loading the view.
    
    NSDictionary *a = [api getRaceGpx:self.raceID];
    NSString* gpxString = [self decodeFromPercentEscapeString:a[@"gpx"]];
    
    GPXParser *gpxParser = [[GPXParser alloc] initWithGPXString:gpxString];
    
    NSArray *track = [gpxParser getTracks];
    NSLog(@"START: lon %@ lat: %@",gpxParser.startLon, gpxParser.startLat);
    self.mapView.delegate=self;
    [self.mapView addOverlays:track];
    
    MKCoordinateRegion region;
    CLLocation *locObj = [[CLLocation alloc] initWithCoordinate:
                          CLLocationCoordinate2DMake(
                                                     [gpxParser.startLat doubleValue],
                                                     [gpxParser.startLon doubleValue]
                                                     )
                                                       altitude:0
                                             horizontalAccuracy:0
                                               verticalAccuracy:0
                                                      timestamp:nil];
    region.center = locObj.coordinate;
    MKCoordinateSpan span;
    span.latitudeDelta  = 0.02; // values for zoom
    span.longitudeDelta = 0.02;
    region.span = span;
    [self.mapView setRegion:region animated:YES];
    
    

}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
	MKPolylineView *polylineView = [[MKPolylineView alloc] initWithPolyline:overlay];
	polylineView.strokeColor = ((GPXTrackPolyLine *)overlay).color;
	polylineView.lineWidth = 1.5;
	return polylineView;
}

- (NSString*) decodeFromPercentEscapeString:(NSString *) string {
    return (__bridge NSString *) CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                                         (__bridge CFStringRef) string,
                                                                                         CFSTR(""),
                                                                                         kCFStringEncodingUTF8);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
