//
//  TRLS_LoginViewController.m
//  Trails
//
//  Created by A. Vatsaev on 04/03/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import "TRLS_LoginViewController.h"
#import "TRLS_ViewController.h"
#import "TRLS_API1.h"



@interface TRLS_LoginViewController ()

@end

@implementation TRLS_LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.api = [TRLS_API1 alloc].init;
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    
	// Do any additional setup after loading the view.
}


- (IBAction)login:(id)sender {
   
    NSDictionary *a = [self.api loginWithUsername:self.loginInput.text andPassword:self.passwdInput.text];
    
    NSLog(@"%@",a);
    
    if( [a[@"status"][@"type"] isEqual:@"ok"] ){
        
        [self.userDefaults setObject:   a[@"user"][@"authtoken"]
                              forKey:   @"userauthtoken"];
        
        [self.userDefaults setObject:   a[@"user"][@"username"]
                              forKey:   @"username"];
        
        [self.userDefaults setObject:   a[@"user"][@"email"]
                              forKey:   @"useremail"];
        
        [self.userDefaults setObject:   a[@"user"][@"first_name"]
                              forKey:   @"userfirstname"];
        
        [self.userDefaults setObject:   a[@"user"][@"last_name"]
                              forKey:   @"userlastname"];
        
        [self.userDefaults setObject:   a[@"user"][@"about"]
                              forKey:   @"userbio"];
        
        [self.userDefaults setObject:   a[@"user"][@"group"]
                              forKey:   @"usergroup"];
                
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    
   //
  
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
