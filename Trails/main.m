//
//  main.m
//  Trails
//
//  Created by A. Vatsaev on 14/02/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TRLS_AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TRLS_AppDelegate class]));
    }
}
