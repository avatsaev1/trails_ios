//
//  TRLS_AppDelegate.h
//  Trails
//
//  Created by A. Vatsaev on 14/02/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRLS_AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
