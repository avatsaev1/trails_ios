//
//  TRLS_API1.h
//  Trails
//
//  Created by A. Vatsaev on 14/02/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRLS_API1 : NSObject{
    
    @private
    NSString* APIHost;
    
    
}

@property (strong, nonatomic) NSString *apiKey;
-(id)initWithApiKey: (NSString*)apiKey;

-(NSArray*)getRaces;
-(NSArray*)getRace: (NSInteger) race_id;
-(NSDictionary*)getRaceGpx: (NSInteger) race_id;
-(NSDictionary*)loginWithUsername:(NSString*)username andPassword:(NSString*)password;
-(NSDictionary*)getParticipations: (NSString *)token;
-(NSDictionary*)acceptParticipationToRace: (NSInteger)race_id withAuthToken:(NSString*)token;
-(NSDictionary*)rejectParticipationToRace: (NSInteger)race_id withAuthToken:(NSString*)token;
-(NSDictionary*)getUserParticipationToRace: (NSInteger)race_id withAuthToken:(NSString*)token;




@end
