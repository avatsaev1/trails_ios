//
//  TRLS_ParticipationsViewController.m
//  Trails
//
//  Created by A. Vatsaev on 06/03/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import "TRLS_ParticipationsViewController.h"

#import "TRLS_RaceViewController.h"

@interface TRLS_ParticipationsViewController ()

@end

@implementation TRLS_ParticipationsViewController{
    NSDictionary* participations;
    NSMutableArray* filteredParticipations;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.api = [TRLS_API1 alloc].init;
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    filteredParticipations = [NSMutableArray alloc].init;
    
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"userauthtoken"] != nil){
        
        participations = [self.api getParticipations:[[NSUserDefaults standardUserDefaults] stringForKey:@"userauthtoken"]];
        [self filter:@"participating"];
        NSLog(@"%@",participations);
        
    }else{
        NSLog(@"user token not found");
        //self.usernameView.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"authtoken"];
    }
    
    
   
	// Do any additional setup after loading the view.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    TRLS_RaceViewController * dst = (TRLS_RaceViewController*)[segue destinationViewController];
    dst.race_id = [filteredParticipations[indexPath.row][@"race"][@"id"] intValue];
    dst.title = filteredParticipations[indexPath.row][@"race"][@"title"];
    dst.description = filteredParticipations[indexPath.row][@"race"][@"description"];
    dst.startTime = filteredParticipations[indexPath.row][@"race"][@"datetime"];
    dst.town = filteredParticipations[indexPath.row][@"race"][@"town"];
    dst.coach = @"";//filteredParticipations[indexPath.row][@"user"][@"username"];
    dst.participationsCount = @"0";//[NSString stringWithFormat:@"%lu",
                               //(unsigned long)[filteredParticipations[indexPath.row][@"race"][@"participations"] count]];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    // Return the number of rows in the section.
    return [filteredParticipations count];
}


-(void)viewDidAppear:(BOOL)animated{
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"userauthtoken"] != nil){
        
        participations = [self.api getParticipations:[[NSUserDefaults standardUserDefaults] stringForKey:@"userauthtoken"]];
        [self filter:@"participating"];
        NSLog(@"%@",participations);
        
    }else{
        NSLog(@"user token not found");
        //self.usernameView.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"authtoken"];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    //cell.textLabel.text = self.races[[NSString stringWithFormat:@"%ld",indexPath.row+1]][@"title"];
    cell.textLabel.text = filteredParticipations[indexPath.row][@"race"][@"title"];
    cell.detailTextLabel.text = filteredParticipations[indexPath.row][@"race"][@"description"];
    
    
    // Configure the cell...
    
    return cell;
}



- (IBAction)filterControlChanged:(id)sender {
    
    NSLog(@"Filter: %ld",(long)self.filterControl.selectedSegmentIndex);
    
    switch (self.filterControl.selectedSegmentIndex) {
        case 0:
            [self filter:@"participating"];
            break;
        case 1:
            [self filter:@"finished"];
            break;
        case 2:
            [self filter:@"pending"];
            break;
        case 3:
            [self filter:@"personal"];
            break;
        default:
            [self filter:@"participating"];
            
    }
}


-(void)filter: (NSString*) tag{
    
    [filteredParticipations removeAllObjects];
    
    if ([@"particpating" isEqual:tag]){

        for (id participation in participations[@"participations"]) {
            //NSLog(@"participation listing: ----- \n%@",participation);
            if ([participation[@"state"] isEqual:@"participating"]) {
                [filteredParticipations addObject:participation];
            }
        }
        
    }else if ([@"finished" isEqual:tag]){
        
        for (id participation in participations[@"participations"]) {
            //NSLog(@"participation listing: ----- \n%@",participation[@"state"]);
            
            if ([participation[@"state"] isEqual:@"finished"]) {
                [filteredParticipations addObject:participation];
            }
        }
        
    }else if ([@"pending" isEqual:tag]){
        
        for (id participation in participations[@"participations"]) {
            //NSLog(@"participation listing: ----- \n%@",participation);
            
            if ([participation[@"state"] isEqual:@"pending"]) {
                [filteredParticipations addObject:participation];
            }
        }
        
    }else if ([@"personal" isEqual:tag]){
        
        for (id participation in participations[@"participations"]) {
           // NSLog(@"participation listing: ----- \n%@",participation);
            
            if ([participation[@"tag"] isEqual:@"personal"]) {
                [filteredParticipations addObject:participation];
            }
        }
    
    }else{
        
        for (id participation in participations[@"participations"]) {
            //NSLog(@"participation listing: ----- \n%@",participation);
            if ([participation[@"state"] isEqual:@"participating"]) {
                [filteredParticipations addObject:participation];
            }
        }
        
    }
    
    NSLog(@"Filtered:\n %@",filteredParticipations);
    [self.tableView reloadData];
}

//This function is where all the magic happens
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    cell.alpha = 0;
    
    
    [UIView beginAnimations:@"appear" context:NULL];
    [UIView setAnimationDuration:0.7];
    
    cell.alpha = 1;
    
    [UIView commitAnimations];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
