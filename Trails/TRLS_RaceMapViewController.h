//
//  TRLS_RaceMapViewController.h
//  Trails
//
//  Created by A. Vatsaev on 08/03/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "GPXParser.h"


@interface TRLS_RaceMapViewController : UIViewController<MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic) NSInteger raceID;



@end
