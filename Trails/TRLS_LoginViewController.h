//
//  TRLS_LoginViewController.h
//  Trails
//
//  Created by A. Vatsaev on 04/03/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRLS_API1.h"

@interface TRLS_LoginViewController : UIViewController

@property (strong, nonatomic) TRLS_API1* api;
@property (strong, nonatomic) IBOutlet UITextField *loginInput;
@property (strong, nonatomic) IBOutlet UITextField *passwdInput;
@property (strong, nonatomic) NSUserDefaults *userDefaults;

@end
