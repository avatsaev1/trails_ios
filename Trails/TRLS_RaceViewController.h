//
//  TRLS_RaceViewController.h
//  Trails
//
//  Created by A. Vatsaev on 15/02/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRLS_API1.h"

@interface TRLS_RaceViewController : UIViewController

@property (strong, nonatomic) TRLS_API1 *api;
@property (strong, nonatomic) NSUserDefaults *userDefaults;
@property (nonatomic) NSInteger race_id;
@property (nonatomic) bool user_participates;
@property (strong, nonatomic) NSString *titleStr;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSString *startTime;
@property (strong, nonatomic) NSString *town;
@property (strong, nonatomic) NSString *coach;
@property (strong, nonatomic) NSString *participationsCount;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *toggleParticipationBtn;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UITextView *descriptionTxt;
@property (strong, nonatomic) IBOutlet UILabel *startTimeLbl;
@property (strong, nonatomic) IBOutlet UILabel *townLbl;
@property (strong, nonatomic) IBOutlet UILabel *coachLbl;
@property (strong, nonatomic) IBOutlet UILabel *participationsCountLbl;


@end
