//
//  TRLS_ParticipationsViewController.h
//  Trails
//
//  Created by A. Vatsaev on 06/03/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRLS_API1.h"

@interface TRLS_ParticipationsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) TRLS_API1* api;
@property (strong, nonatomic) NSUserDefaults *userDefaults;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *filterControl;

@end
