//
//  TRLS_RacesViewController.h
//  Trails
//
//  Created by A. Vatsaev on 15/02/2014.
//  Copyright (c) 2014 Vatsaev Aslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRLS_API1.h"

@interface TRLS_RacesViewController : UITableViewController

@property (strong, nonatomic) TRLS_API1* api;
@property (strong, nonatomic) NSArray* races;

@end
